package org.pixie.bencoding.bencode.types;

import org.pixie.bencoding.bencode.BElement;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Project Bencoding
 * Created by Francis on 27/03/14.
 * 
 * Represents a Bencode string
 */
public class BString implements BElement {
    /**
     * The BenDeCoded string.
     */
    public String value;

    /**
     * @param value
     */
    public BString(final String value) {
        this.value = value;
    }

    /**
     * @return The value of the BenDeCoded string.
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value
     */
    public void setValue(final String value) {
        this.value = value;
    }

    @Override
    public String encode() {
        return value.length() + ":" + value;
    }

    /**
     * @param encoded the string we are decoding
     * @param index the index to read from
     * @return The BenDeCoded string.
     */
    public static BString read(final String encoded, AtomicInteger index) {
        final int colonIndex = encoded.indexOf(':', index.get());
        final int length = Integer.parseInt(encoded.substring(index.get(), colonIndex));
        index.set(colonIndex + 1);
        final String value = encoded.substring(index.get(), index.get() + length);
        index.set(index.get() + length);
        return new BString(value);
    }

    @Override
    public String toString() {
        return value;
    }
}
