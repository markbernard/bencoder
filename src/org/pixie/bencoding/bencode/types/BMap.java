package org.pixie.bencoding.bencode.types;

import org.pixie.bencoding.bencode.BElement;
import org.pixie.bencoding.bencode.BReader;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Project Bencoding
 * Created by Francis on 27/03/14.
 * 
 * Represents a Bencode map/dictionary
 */
public class BMap extends HashMap<BString, BElement> implements BElement {
    private static final long serialVersionUID = 3348853009090661023L;

    @Override
    public String encode() {
        final StringBuilder builder = new StringBuilder();
        builder.append('d');
        for (final Map.Entry<BString, BElement> entry : entrySet()) {
            builder.append(entry.getKey().encode() + entry.getValue().encode());
        }
        return builder.append('e').toString();
    }

    /**
     * @param encoded the string we are decoding
     * @param index the index to read from
     * @return A BenDeCoded map.
     */
    public static BMap read(final String encoded, final AtomicInteger index) {
        if (encoded.charAt(index.get()) == 'd') index.set(index.get() + 1);
        final BMap map = new BMap();
        while (encoded.charAt(index.get()) != 'e') {
            final BString key = BString.read(encoded, index);
            final BElement value = BReader.read(encoded, index);
            map.put(key, value);
        }
        index.set(index.get() + 1);
        return map;
    }
}
