package org.pixie.bencoding.bencode.types;

import org.pixie.bencoding.bencode.BElement;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Project Bencoding
 * Created by Francis on 27/03/14.
 * 
 * Represents a Bencode number
 */
public class BNumber implements BElement {
    /**
     * The number BenDeCoded value
     */
    public long value;

    /**
     * @param value
     */
    public BNumber(final int value) {
        this.value = value;
    }

    /**
     * @return The value of the BenDeCoded number.
     */
    public long getValue() {
        return value;
    }

    /**
     * @param value
     */
    public void setValue(final long value) {
        this.value = value;
    }

    @Override
    public String encode() {
        return "i" + value + "e";
    }

    /**
     * @param encoded
     * @param index
     * @return The BenDeCoded number.
     */
    public static BNumber read(final String encoded, final AtomicInteger index) {
        if (encoded.charAt(index.get()) == 'i') index.set(index.get() + 1);
        final int end = encoded.indexOf('e', index.get());
        final int value = Integer.valueOf(encoded.substring(index.get(), end));
        index.set(end + 1);
        return new BNumber(value);
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
