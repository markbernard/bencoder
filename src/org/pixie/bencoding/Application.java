package org.pixie.bencoding;

import org.pixie.bencoding.bencode.BElement;
import org.pixie.bencoding.bencode.BReader;

import java.util.Arrays;

/**
 * Project Bencoding
 * Created by Francis on 26/03/14.
 */
public class Application {

    /**
     * Test decoder.
     * 
     * @param args 
     */
    public static void main(final String[] args) {
        final BElement[] elements3 = BReader.read("i523e5:abcdel4:spam4:eggsed3:cow3:moo4:spam15:thisisalongworde");
        System.out.println(Arrays.toString(elements3));
    }

}
